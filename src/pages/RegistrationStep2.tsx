import React, { useRef, useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonDatetime,
  IonGrid,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";

const RegistrationStep2: React.FC = () => {
  const [disableLoginBtn, setdisableLoginBtn] = useState(true);

  const handleChange = (event:any) => {
    if (event) setdisableLoginBtn(false);
     else setdisableLoginBtn(true);
   }
  const dobRef = useRef<HTMLIonDatetimeElement>(null);
  const [age, ageChange] = useState<number>(0);

  const calculateAge = () => {
    var calAge: number = 0;
    var calMonth: number = 0;

    const dobString: string = dobRef.current!.value!;
    const dob: Date = new Date(dobString);
    const yearDob: number = dob.getFullYear();
    const monthDob: number = dob.getMonth();

    const today: Date = new Date();
    const yearNow: number = today.getFullYear();
    const monthNow: number = today.getMonth();

    calAge = yearNow - yearDob;
    calMonth = monthNow - monthDob;

    if (calMonth < 0 || (calMonth === 0 && today.getDate() < dob.getDate())) {
      calAge--;
    }

    if (calAge < 0) calAge = 0;

    ageChange(calAge);
    console.log(age);
  };
  return (
    <IonPage>
      <IonContent class="ionContent">
      <IonGrid class="headergrid">
          <IonRow>
           <IonBackButton defaultHref="RegistrationStep1" color="light" class="backbutton"/>
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>REGISTRATION</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
          </IonGrid>
          <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card">
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>STEP-2</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        <IonLabel class="inputlabel">First Name</IonLabel>
                        <IonInput
                          placeholder="First Name"
                          clear-input={true}
                          autofocus={true}
                          type="text"
                        ></IonInput>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonLabel class="inputlabel">Last Name</IonLabel>
                        <IonInput
                          placeholder="Last Name"
                          clear-input={true}
                          autofocus={true}
                          type="text"
                          onIonChange={e => handleChange(e.detail.value!)}
                        ></IonInput>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol size="9">
                        <IonLabel class="inputlabel">Date Of Birth</IonLabel>
                        <IonDatetime
                          class="dob"
                          ref={dobRef}
                          placeholder="Select Date"
                          displayFormat="MMM DD, YYYY"
                          onIonChange={calculateAge}
                        ></IonDatetime>
                      </IonCol>
                      <IonCol size="3">
                        <IonLabel class="inputlabel">Age</IonLabel>
                        <IonInput class="dob" value={age}></IonInput>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        routerLink="/RegistrationStep3"
                        class="nextBtn"
                        strong={true}
                        disabled={disableLoginBtn}
                      >
                        NEXT
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep2;
