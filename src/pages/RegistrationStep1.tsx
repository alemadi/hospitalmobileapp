import React, { useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";
import "./Login.css";

const RegistrationStep1: React.FC = () => {
  const [disableLoginBtn, setdisableLoginBtn] = useState(true);

  const handleChange = (event: any) => {
    if (event) setdisableLoginBtn(false);
    else setdisableLoginBtn(true);
  };
  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="headergrid">
          <IonRow>
            <IonBackButton
              defaultHref="Login"
              color="light"
              class="backbutton"
            />{" "}
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>REGISTRATION</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="card">
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>STEP-1</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">Enter QID Number</IonLabel>
                        <IonInput
                          placeholder="QID Number"
                          clear-input={true}
                          autofocus={true}
                          type="number"
                        ></IonInput>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          Enter Mobile Number
                        </IonLabel>
                        <IonInput
                          placeholder="Mobile Number"
                          clear-input={true}
                          autofocus={true}
                          type="number"
                          onIonChange={(e) => handleChange(e.detail.value!)}
                        ></IonInput>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        routerLink="/RegistrationStep2"
                        class="nextBtn"
                        strong={true}
                        disabled={disableLoginBtn}
                      >
                        NEXT
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep1;
