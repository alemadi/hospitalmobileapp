import {
  IonCol,
  IonContent,
  IonGrid,
  IonLabel,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonSearchbar,
} from "@ionic/react";
import React, { useState } from "react";
import Header from "./Header";
import Footer from "./Footer";
import location from "../images/location.png";
import FindDoctorPageIcone01 from "../images/FindDoctorPageIcone-01.png";
import FindDoctorPageIcone02 from "../images/FindDoctorPageIcone-02.png";
import FindDoctorPageIcone03 from "../images/FindDoctorPageIcone-03.png";
import FindDoctorPageIcone04 from "../images/FindDoctorPageIcone-04.png";
import FindDoctorPageIcone05 from "../images/FindDoctorPageIcone-05.png";
import FindDoctorPageIcone06 from "../images/FindDoctorPageIcone-06.png";
import FindDoctorPageIcone07 from "../images/FindDoctorPageIcone-07.png";
import FindDoctorPageIcone08 from "../images/FindDoctorPageIcone-08.png";
import FindDoctorPageIcone09 from "../images/FindDoctorPageIcone-09.png";
import FindDoctorPageIcone10 from "../images/FindDoctorPageIcone-10.png";
import FindDoctorPageIcone11 from "../images/FindDoctorPageIcone-11.png";
import FindDoctorPageIcone12 from "../images/FindDoctorPageIcone-12.png";
import FindDoctorPageIcone13 from "../images/FindDoctorPageIcone-13.png";
import FindDoctorPageIcone14 from "../images/FindDoctorPageIcone-14.png";
import FindDoctorPageIcone15 from "../images/FindDoctorPageIcone-15.png";
import FindDoctorPageIcone16 from "../images/FindDoctorPageIcone-16.png";

import "./FindDoctor.css";

const FindDoctor: React.FC = () => {
  const [searchText, setSearchText] = useState("");

  interface Idetails {
    icon: any;
    url: string;
  }
  const arrayOfServices: Array<Idetails> = [
    {
      icon: FindDoctorPageIcone01,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone02,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone03,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone04,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone05,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone06,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone07,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone08,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone09,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone10,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone11,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone12,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone13,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone14,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone15,
      url: "/FindDoctor",
    },
    {
      icon: FindDoctorPageIcone16,
      url: "/FindDoctor",
    },
  ];
  return (
    <IonPage>
      <Header title={"Find Doctor"} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="grid">
          <IonRow>
            <IonCol size="2" class="locationCol">
              <img className="location" src={location} alt="icon missing"></img>
            </IonCol>
            <IonCol>
              <IonLabel class="selectlocation">Select Location</IonLabel>
              <br></br>
              <IonRadioGroup value="AlHilalWest">
                <IonCol class="radioStyle" className="radiobutton">
                  <IonRadio value="AlHilalWest" class="radioposition" />
                  <IonLabel>
                    {" "}
                    <b>AL HILAL WEST</b>
                  </IonLabel>
                </IonCol>
                <IonRow>
                  <IonCol class="radioStyle" className="radiobutton">
                    <IonRadio value="NorthArRayyan" class="radioposition" />
                    <IonLabel>
                      <b> NORTH, AR-RAYYAN</b>
                    </IonLabel>
                  </IonCol>
                </IonRow>
              </IonRadioGroup>
            </IonCol>
          </IonRow>
          <br />

          <IonRow class="divider">
            <IonSearchbar
              value={searchText}
              onIonChange={(e) => setSearchText(e.detail.value!)}
              placeholder="Search Speciality"
              animated
              class="searchBar"
            ></IonSearchbar>
          </IonRow>
          <IonRow>
            {arrayOfServices.map((item) => (
              /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
              <IonCol key={item.icon} size="4" class="colContent">
                <a href={item.url}>
                  <img src={item.icon} alt="icon is missing"></img>
                </a>
              </IonCol>
            ))}
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default FindDoctor;
