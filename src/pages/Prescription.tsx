import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import Doctor from "../images/Doctor-01.png";
import "./Prescription.css";

const Prescription: React.FC = () => {
  interface Idetails {
    name: string;
    department: string;
    type: string;
    lastVist: string;
  }

  const arrayOfDetails: Array<Idetails> = [
    {
      name: "DR. ABDULHAK SADALLA1",
      department: "INTERNAL MEDICINE",
      lastVist: "15 APRIL 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
    {
      name: "DR. ABDULHAK SADALLA2",
      department: "INTERNAL MEDICINE",
      lastVist: "15 APRIL 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
    {
      name: "DR. ABDULHAK SADALLA3",
      department: "INTERNAL MEDICINE",
      lastVist: "15 APRIL 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
  ];
  return (
    <IonPage>
      <Header title={"Prescription"} defaultRoute={"/Main"} />
      <IonContent>
        {arrayOfDetails.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.name} class="p-cardStyle">
            <IonCardContent class="p-cardContentStyle">
              <IonGrid>
                <IonRow class="p-firstRow">
                  <IonCol class="p-firstRowCol">
                    <IonText>
                      <b>DATE: {item.lastVist}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="p-secondRow">
                  <IonCol class="p-secondRowCol">
                    <IonText>
                      <b>DOCTOR BY</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="p-firstRow">
                  <IonCol size="2">
                    <IonImg src={Doctor}></IonImg>
                  </IonCol>
                  <IonCol size="10" class="p-textCol">
                    <IonText class="p-text1">
                      <b>{item.name}</b>
                    </IonText>
                    <IonText class="p-text2">{item.department}</IonText>
                    <IonText class="p-text3">{item.type}</IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButton class="p-btn">
                      <b>View Prescription</b>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Prescription;
