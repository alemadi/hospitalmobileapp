import {
  IonAvatar,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCheckbox,
  IonCol,
  IonContent,
  IonGrid,
  IonIcon,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
  useIonRouter,
} from "@ionic/react";
import React, { useRef, useState } from "react";
import "./Login.css";
import logo from "../images/defaultProfileImage.png";
import axios from "axios";
import { checkmarkCircleOutline } from "ionicons/icons";
import { endpoints, apiPath } from "../shared/application_constants";
import { Router } from "workbox-routing";

const Login1: React.FC = () => {
  const [disableLoginBtn, setdisableLoginBtn] = useState(true);
  const [otpArray, setOtpArray] = useState(["", "", "", ""]);
  const [IsValidQID, setIsValidQID] = useState(true);
  const [patcode, setPatcode] = useState(null);
  const router = useIonRouter();

  // shared
  console.log(
    "[Login page]: printing endpoint",
    endpoints.hostName,
    apiPath.loginApiPath
  );

  const handleChange = (event: any) => {
    if (event) setdisableLoginBtn(false);
    else setdisableLoginBtn(true);
  };
  const userNameInputRef = useRef<HTMLIonInputElement>(null);
  let userName: string;

  const getOtp = () => {
    const otpValues = Object.values(otpArray);
    console.log(otpArray.toString().replaceAll(",", "").length);
    userName = userNameInputRef.current!.value!.toLocaleString();
    console.log(userName);
  };

  const ValidateQID = (event: any) => {
    axios
      .post("http://34.131.82.122:3001/alemadi/mobile/patient/otp/send", {
        qatarId: 28535659260,
      })
      .then(
        (response) => {
          console.log(response);
          setIsValidQID(false);
        },
        (error) => {
          console.log(error);
          setIsValidQID(true);
        }
      );
    console.log(userName);
    const pattern3 = /^[2-3][0-9.,]/;
    if (!userName || userName == "" || !pattern3.test(userName)) {
      console.log("invalid qid");
      return;
    }
    let reqdata = {
      // "qatarId": 28535659269
      qatarId: userName,
    };
    axios.post(endpoints.hostName + apiPath.sendOtpApiPath, reqdata).then(
      (response: any) => {
        if (response && response.data && response.data["PATCODE"]) {
          setPatcode(response.data["PATCODE"]);
        }
        console.log("[Login page]: generate otp successful");
      },
      (error) => {
        console.log("[Login page]: generate otp Error");
      }
    );
  };

  const verifyOtp = () => {
    let otpString = otpArray.toString().replaceAll(",", "");
    if (!otpString || otpString == "" || otpString.length !== 4) {
      console.log("otp not entered");
      return;
    }
    let data = {
      PATCODE: patcode,
      OTP: otpString,
    };
    axios.post(endpoints.hostName + apiPath.verifyOtpApiPath, data).then(
      (response) => {
        if (response && response.data) {
          console.log("[Login page]: otp verification Pass");
          router.push("/main", "none", "replace");
        } else {
          console.log("[Login page]: otp verification return false");
        }
      },
      (error) => {
        console.log("[Login page]: otp verification Error");
      }
    );
  };

  const AuthenticateUser = () => {
    userName = userNameInputRef.current!.value!.toLocaleString();
    // password = passwordInputRef.current!.value!.toLocaleString();
    const valuesOnly = Object.values(otpArray);
    console.log(valuesOnly.toString().replaceAll(",", "").length);
    console.log(userName);
  };

  const numberOnlyValidation = (event: any) => {
    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  };
  const QIdValidation = (event: any) => {
    const pattern2 = /[0-9.,]/;
    const pattern3 = /^[2-3][0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);
    console.log(pattern3.test(event.target.value));
    if (!event.target.value || event.target.value == "") {
      if (inputChar != "2" && inputChar != "3") {
        event.preventDefault();
      }
    } else if (!pattern2.test(inputChar)) {
      event.preventDefault();
    }
  };

  //change otp
  const onChange = (event: any) => {
    let index = event.target.id;
    console.log(index);
    if (event.target.value) {
      const otpArrayCopy = otpArray.concat();
      otpArrayCopy[Number(index)] = event.target.value;
      setOtpArray(otpArrayCopy);
      if (index === "0") {
        document.getElementById("1")?.focus();
      } else if (index === "1") {
        document.getElementById("2")?.focus();
      } else if (index === "2") {
        document.getElementById("3")?.focus();
        // do the rest here
      }
    } else {
      const otpArrayCopy = otpArray.concat();
      otpArrayCopy[Number(index)] = ""; // clear the previous box which will be in focus
      setOtpArray(otpArrayCopy);
    }
  };

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol>
              <h3 className="loginLabel">
                <IonText> LOGIN TO</IonText>
              </h3>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="description">Al Emadi Hospital - Doha, Qatar</IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonCard class="card">
                <IonCardHeader>
                  <IonAvatar class="profileImage-center">
                    <img src={logo} alt="logo missing" />
                  </IonAvatar>
                </IonCardHeader>
                <IonCardContent class="cardContent">
                  <IonGrid>
                    <IonRow>
                      <IonCol size="10" offset="1">
                        <IonLabel class="inputlabel">QID Number</IonLabel>
                        <IonInput
                          type="tel"
                          placeholder="QID Number"
                          clear-input={true}
                          autofocus={true}
                          ref={userNameInputRef}
                          pattern="/[0-9.,]/"
                          maxlength={11}
                          onIonChange={(e) => handleChange(e.detail.value!)}
                          onKeyPress={QIdValidation}
                        >
                          <label hidden={IsValidQID}>
                            <div>
                              <IonIcon
                                icon={checkmarkCircleOutline}
                                slot="end"
                                size="large"
                                className="checkmark"
                              />
                            </div>
                          </label>
                        </IonInput>
                      </IonCol>
                      <IonCol size="10" offset="1">
                        <IonLabel class="inputlabel">Mobile Number</IonLabel>
                        <IonInput
                          placeholder="Mobile Number"
                          clear-input={true}
                          autofocus={true}
                          ref={userNameInputRef}
                          type="tel"
                          maxlength={10}
                        ></IonInput>
                        <IonButton size="small" class="otpbtn" onClick={getOtp}>
                          Generate OTP
                        </IonButton>
                      </IonCol>
                      <br />
                      <br />
                      <IonCol size="10" offset="1" class="center">
                        <IonLabel class="inputlabel">OTP Number</IonLabel>
                        <input
                          value={otpArray[0]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="0"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[1]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="1"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[2]}
                          className="otpInput"
                          maxLength={1}
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="2"
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[3]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="3"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        {/* <IonInput
                          class="otpInput"
                          placeholder="0"
                          type="tel"
                          maxlength={1}
                          value={otpArray[0]}
                          id="0"
                          onIonChange={onChange}
                          onKeyPress={numberOnlyValidation} 
                          ref={firstTextInputRef}
                        ></IonInput>
                        <IonInput
                          class="otpInput"
                          placeholder="0"
                          type="tel"
                          maxlength={1}
                          value={otpArray[1]}
                          id="1"
                          onIonChange={onChange}
                          onKeyPress={numberOnlyValidation}
                          ref={secondTextInputRef}
                        ></IonInput>
                        <IonInput
                          class="otpInput"
                          placeholder="0"
                          type="tel"
                          maxlength={1}
                          value={otpArray[2]}
                          id="2"
                          onIonChange={onChange}
                          onKeyPress={numberOnlyValidation}
                        ></IonInput>
                        <IonInput
                          class="otpInput"
                          placeholder="0"
                          type="tel"
                          maxlength={1}
                          value={otpArray[3]}
                          id="3"
                          onIonChange={onChange}
                          onKeyPress={numberOnlyValidation}
                        ></IonInput> */}
                      </IonCol>
                    </IonRow>
                    <br></br>
                    <IonRow class="fontsize">
                      <IonCol size="6">
                        <IonCheckbox
                          checked={true}
                          class="checkBox"
                        ></IonCheckbox>
                        {"    "}
                        <IonLabel> Remember me</IonLabel>
                      </IonCol>
                    </IonRow>
                    <br></br>
                    <IonRow>
                      <IonCol class="loginbutton">
                        <IonButton
                          expand="block"
                          onClick={verifyOtp}
                          // routerLink="/Main"
                          disabled={disableLoginBtn}
                        >
                          LOGIN
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <h3 className="h2BottomText">
                <a href="/RegistrationStep1" className="anchorWhite">
                  <IonText> OR REGISTER HERE</IonText>
                </a>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login1;
