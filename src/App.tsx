import { Redirect, Route } from "react-router-dom";
import { IonApp, IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import Login from "./pages/Login";
import ForgotPassword from "./pages/ForgotPassword";
import Main from "./pages/Main";
import FindDoctor from "./pages/FindDoctor";
import BookAnAppointmentStep1 from "./pages/BookAnAppointmentStep1";
import BookAnAppointmentStep2 from "./pages/BookAnAppointmentStep2";
import BookAnAppointmentStep3 from "./pages/BookAnAppointmentStep3";
import RegistrationStep1 from "./pages/RegistrationStep1";
import RegistrationStep2 from "./pages/RegistrationStep2";
import RegistrationStep3 from "./pages/RegistrationStep3";
import RegistrationStep4 from "./pages/RegistrationStep4";
import InsuranceApproval from "./pages/InsuranceApproval";
import Prescription from "./pages/Prescription";
import YourDoctors from "./pages/YourDoctors";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import React from "react";

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/Login">
          <Login />
        </Route>
        <Route exact path="/ForgotPassword">
          <ForgotPassword />
        </Route>
        <Route exact path="/Main">
          <Main />
        </Route>
        <Route exact path="/FindDoctor">
          <FindDoctor />
        </Route>
        <Route exact path="/BookAnAppointmentStep1">
          <BookAnAppointmentStep1 />
        </Route>
        <Route exact path="/BookAnAppointmentStep2">
          <BookAnAppointmentStep2 />
        </Route>
        <Route exact path="/BookAnAppointmentStep3">
          <BookAnAppointmentStep3 />
        </Route>
        <Route exact path="/RegistrationStep1">
          <RegistrationStep1 />
        </Route>
        <Route exact path="/RegistrationStep2">
          <RegistrationStep2 />
        </Route>
        <Route exact path="/RegistrationStep3">
          <RegistrationStep3 />
        </Route>
        <Route exact path="/RegistrationStep4">
          <RegistrationStep4 />
        </Route>
        <Route exact path="/InsuranceApproval">
          <InsuranceApproval />
        </Route>
        <Route exact path="/YourDoctors">
          <YourDoctors />
        </Route>
        <Route exact path="/Prescription">
          <Prescription />
        </Route>

        <Redirect path="/" to="/Login" exact />
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
